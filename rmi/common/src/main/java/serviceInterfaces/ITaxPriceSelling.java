package serviceInterfaces;

import entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ITaxPriceSelling extends Remote{

    double computeSellingPrice(Car c) throws RemoteException;

    double computeTax(Car c) throws RemoteException;
}
