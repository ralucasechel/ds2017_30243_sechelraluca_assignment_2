package entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Car implements Serializable {

    private static final long serialVersionUID = 1L;
    private int year;
    private int engineCapacity;
    private double price;

    public Car(int year, int engineCapacity) {
        this.year = year;
        this.engineCapacity = engineCapacity;
    }

    public Car(int year, double price) {
        this.year = year;
        this.price = price;
    }
}
