package communication;

import gui.Controller;
import gui.View;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class RMIClient {

    public static void main(String[] args) throws RemoteException, NotBoundException {

        View view = new View();
        Controller controller = new Controller(view);

    }

}
