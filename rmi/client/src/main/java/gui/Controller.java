package gui;

import entities.Car;
import serviceInterfaces.ITaxPriceSelling;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Controller {

    private View view;
    private ITaxPriceSelling taxPrice;

    public Controller(View view) throws RemoteException, NotBoundException {

        Registry registry = LocateRegistry.getRegistry("", 1099);
        taxPrice = (ITaxPriceSelling) registry.lookup("TaxPrice");

        this.view = view;
        this.view.setVisible(true);
        this.view.setResizable(false);

        this.view.addBtnTaxActionListener(new TaxActionListener());
        this.view.addBtnPriceActionListener(new PriceActionListener());
    }


    class TaxActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String year = view.getYear();
            String engine = view.getEngine();
            String price = view.getPrice();

            try {
                Car car = new Car();
                car.setYear(Integer.parseInt(year));
                car.setEngineCapacity(Integer.parseInt(engine));
                car.setPrice(Double.parseDouble(price));

                double tax = taxPrice.computeTax(car);
                view.writeText(String.valueOf(tax));

            } catch (NumberFormatException ex) {
                view.writeText("Please enter correct values!");
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
        }
    }


    class PriceActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String year = view.getYear();
            String engine = view.getEngine();
            String price = view.getPrice();

            if (!("".equals(year) || "".equals(engine) || "".equals(price))) {
                Car car = new Car();
                car.setYear(Integer.parseInt(year));
                car.setEngineCapacity(Integer.parseInt(engine));
                car.setPrice(Double.parseDouble(price));

                try {
                    double sellingPrice = taxPrice.computeSellingPrice(car);
                    view.writeText(" Selling Price = " + sellingPrice);
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            } else {
                view.writeText("Please fill all text boxes before submiting!");
            }
        }
    }
}
