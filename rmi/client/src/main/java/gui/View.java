package gui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

public class View extends JFrame {

    private JTextField textYear;
    private JTextField textEngine;
    private JTextField textPrice;

    private JButton btnTax;
    private JButton btnPrice;

    private JTextArea textArea;

    public View() {
        setTitle("Java RMI application");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(300, 100, 400, 300);
        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(panel);
        panel.setLayout(null);

        JLabel info = new JLabel("Insert car information:");
        info.setBounds(10, 11, 160, 14);

        JLabel year = new JLabel("Fabrication year");
        year.setBounds(25, 36, 120, 20);

        JLabel engine = new JLabel("Engine size");
        engine.setBounds(25, 61, 120, 14);

        JLabel price = new JLabel("Purchasing price");
        price.setBounds(25, 86, 120, 14);

        textYear = new JTextField();
        textYear.setBounds(150, 33, 86, 20);
        textYear.setColumns(10);

        textEngine = new JTextField();
        textEngine.setBounds(150, 58, 86, 20);
        textEngine.setColumns(10);

        textPrice = new JTextField();
        textPrice.setBounds(150, 83, 86, 20);
        textPrice.setColumns(10);

        btnTax = new JButton("Compute Tax");
        btnTax.setBounds(20, 132, 150, 23);

        btnPrice = new JButton("Selling Price");
        btnPrice.setBounds(20, 162, 150, 23);

        textArea = new JTextArea();
        textArea.setBounds(240, 100, 150, 150);


        panel.add(info);
        panel.add(year);
        panel.add(engine);
        panel.add(price);
        panel.add(textYear);
        panel.add(textEngine);
        panel.add(textPrice);
        panel.add(btnTax);
        panel.add(btnPrice);
        panel.add(textArea);
    }

    void addBtnPriceActionListener(ActionListener e) {
        btnPrice.addActionListener(e);
    }

    void addBtnTaxActionListener(ActionListener e) {
        btnTax.addActionListener(e);
    }

    String getYear() {
        return textYear.getText();
    }

    String getEngine() {
        return textEngine.getText();
    }

    String getPrice() {
        return textPrice.getText();
    }

    void writeText(String s){
        textArea.append(s);
        textArea.append("\n");
    }

    public void clear() {
        textYear.setText("");
        textEngine.setText("");
        textPrice.setText("");
        textArea.setText("");
    }
}
