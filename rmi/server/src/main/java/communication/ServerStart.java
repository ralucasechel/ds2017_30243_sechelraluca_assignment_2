package communication;

import service.TaxPriceSelling;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


public class ServerStart {
    public static void main(String[] args) throws RemoteException, AlreadyBoundException {

        TaxPriceSelling taxPrice = new TaxPriceSelling();

        Registry reg = LocateRegistry.createRegistry(1099);
        reg.bind("TaxPrice", taxPrice);

        System.out.println("Server start");

    }

}
